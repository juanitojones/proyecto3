#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct tabla{ // estructura que se usara para almacenar los datos del archivo CARRERA.TXT
    char carrera[100];
    int codigo;
    int NEM;
    int rank;
    int LENG;
    int MAT;
    int HIST;
    int CS;
    int MINPOND;
    int MINPSU;
    float PRIM;
    float ULT;
    int PSU;
    int BEA;
    char facultad[100];
};
int menu(){ // menu que mostrara las opciones que puede ingresar el usuario
    int x;
    printf("menu\n");
    printf("1) consultar ponderacion carrera\n");
    printf("2) simular postulacion carrera\n");
    printf("3) mostrar ponderaciones facultad\n");
    printf("4) salir \n");
    scanf("%d",&x);
    return x;
}
void almacenarmatriz(struct tabla carreras[]){ // funcion que almacena los datos del archivo de texto y los guarda
    FILE *file;                                 // en la estructura carreras[]
    char linea[1024];
    int i = 0;
    file = fopen("CARRERAS.txt","r");
    if (file == NULL)
        printf("error al abrir archivo \n");
        
    else
    {
        fgets(linea,1024,file); // los datos relevantes del archivo de texto estan separados por espacio
        while(fgets(linea,1024,file) != NULL)//por lo que en el nombre de una carrera se remplaza el espacio por "_"
        {
            sscanf(linea,"%s %d %d %d %d %d %d %d %d %d %f %f %d %d %s",carreras[i].carrera,&carreras[i].codigo,&carreras[i].NEM,&carreras[i].rank,&carreras[i].LENG,&carreras[i].MAT,&carreras[i].HIST,&carreras[i].CS,&carreras[i].MINPOND,&carreras[i].MINPSU,&carreras[i].PRIM,&carreras[i].ULT,&carreras[i].PSU,&carreras[i].BEA,carreras[i].facultad);
            i++;
        }
    }
    fclose(file);
}
void ponderacioncarrera(struct tabla carreras[]){//funcion que muestra los datos de una carrera segun el codigo que
    int i=0,x=0;                                //ingresa el usuario
    printf("lista carreras y su codigo : \n");
    for(i=0;i<53;i++)
        printf("%s codigo : %d\n",carreras[i].carrera,carreras[i].codigo);
    while(x != 1)
    {
        printf("ingrese el codigo \ningrese 1 para salir\n");
        scanf("%d",&x);
        for(i=0;i<53;i++)
        {
            if(x == carreras[i].codigo)
            {
                printf("%s - %s \n",carreras[i].carrera,carreras[i].facultad);
                printf("NEM:%d\nRANKING:%d\nLENGUAJE:%d\nMATEMATICA:%d\n",carreras[i].NEM,carreras[i].rank,carreras[i].LENG,carreras[i].MAT);
                if (carreras[i].HIST == 0)
                    printf("HISTORIA : \n");
                else
                    printf("HISTORIA : %d\n",carreras[i].HIST);
                if (carreras[i].CS == 0)
                    printf("CIENCIAS: \n");
                else
                    printf("CIENCIAS: %d\n",carreras[i].CS);
                if(carreras[i].ULT == 0)
                    printf("ultimo seleccioado:S/I malla nueva\nPor PSU:%d\n Por BEA:%d\n",carreras[i].PSU,carreras[i].BEA);
                else
                    printf("Último seleccionado:%.1f\nCupos:\n   Por PSU:%d\n Por BEA:%d\n",carreras[i].ULT,carreras[i].PSU,carreras[i].BEA);
            }
        }
    }
}
void consultafacultad(struct tabla carreras[]){//funcion que muestra las carreras de una facultad que consulte el 
    int x = 0,i=0,min=0,max=0;                  //usuario. 
    printf("seleccione la facultad que desea revisar:\n");
    printf("1)FACULTAD DE ARQUITECTURA\n2)FACULTAD DE CIENCIAS\n");
    printf("3)FACULTAD DE CIENCIAS DEL MAR Y DE RECURSOS NATURALES\n");
    printf("4)FACULTAD DE CIENCIAS ECONÓMICAS Y ADMINISTRATIVAS\n");
    printf("5)FACULTAD DE DERECHO Y CIENCIAS SOCIALES\n");
    printf("6)FACULTAD DE FARMACIA\n7)FACULTAD DE HUMANIDADES\n8)FACULTAD DE INGENIERÍA\n");
    printf("9)FACULTAD DE MEDICINA\n10)FACULTAD DE ODONTOLOGÍA\n");
    printf("11 para salir \n");
    while (x != 11)
    {
        printf("ingrese numero: \n");
        scanf("%d",&x);
        switch(x)//se utilizan intervalos entre min y max para mostrar las carreras de una facultad consultada 
        {
            case 1:
                min = 0;
                max = 5;
                break;
            case 2:
                min=5;
                max=9;
                break;
            case 3:
                min=9;
                max=10;
                break;
            case 4:
                min=10;
                max=21; 
                break;
            case 5:
                min=21;
                max=23;
                break;
            case 6:
                min=23;
                max=25;
                break;
            case 7:
                min=25;
                max=29;
                break;
            case 8:
                min=29;
                max=39;
                break;
            case 9:
                min=39;
                max=51;
                break;
            case 10:
                min=51;
                max=52;
                break;
        }
        printf("min %d max %d\n",min,max);
        for(i=min;i<max;i++)
            printf("%s %d %d %d %d %d %d %d %d %d %.1f %.1f %d %d \n",carreras[i].carrera,carreras[i].codigo,carreras[i].NEM,carreras[i].rank,carreras[i].LENG,carreras[i].MAT,carreras[i].HIST,carreras[i].CS,carreras[i].MINPOND,carreras[i].MINPSU,carreras[i].PRIM,carreras[i].ULT,carreras[i].PSU,carreras[i].BEA);
    }
}
void simularpostulacion(struct tabla carreras[]){//esta funcion simula la postulacion de una carrera 
    int x=0,nem,ranking,lenguaje,matematica,historia,ciencias,i;
    for(i=0;i<53;i++)
            printf("%d) - carrera: %s\n",i,carreras[i].carrera);
    while (x != 100)//el usuario ingresa sus puntajes y calcula su puntaje ponderado 
    {
        float puntajepond;
        printf("seleccione la carrera el cual desea postular: \n");
        printf("ingrese 100 para salir\n");
        scanf("%d",&x);//se indican el numero de la carrera el cual el usuario debe ingresar para calcular su puntaje
        if((x < 0)&&(x>53))
            printf("error,seleccione la carrera correcta\n");
        else
        {
            printf("ingrese su NEM\n");
            scanf("%d",&nem);
            printf("ingrese su ranking\n");
            scanf("%d",&ranking);
            printf("ingrese su puntaje en lenguaje\n");
            scanf("%d",&lenguaje);
            printf("ingrese su puntaje en matematica\n");
            scanf("%d",&matematica);
            if(carreras[x].HIST == 0)
            {
                ciencias = 0;
                printf("ingrese su puntaje en historia\n");
                scanf("%d",&historia);
            }
            else
            {
                if(carreras[x].CS == 0)
                {
                    historia = 0;
                    printf("ingrese su puntaje en ciencias\n");
                    scanf("%d",&ciencias);
                }
                else
                {
                    printf("ingrese puntaje ciencias o historia\n");
                    scanf("%d",&historia);
                    ciencias = 0;
                }
            }
            puntajepond = (nem*carreras[x].NEM + ranking*carreras[x].rank + lenguaje*carreras[x].LENG + matematica*carreras[x].MAT + historia*carreras[x].HIST + ciencias*carreras[x].CS)/100;
            printf("%s - %s\nultimo seleccionado:%.1f\n",carreras[x].carrera, carreras[x].facultad,carreras[x].ULT);
            printf("tu puntaje : %.1f\n",puntajepond);
            if(puntajepond > carreras[x].ULT)
                printf("tu puntaje es mayor al del ultimo seleccionado\nestas dentro del rango de seleccion\n");
            else
                if(puntajepond< carreras[x].ULT)
                    printf("tu puntaje es menor al del ultimo seleccionado\nestas fuera del rango de seleccion\n");
                else
                    printf("tu puntaje es similar al del ultimo seleccionado\n");
        }
    }
}
    
int main(){
    int opcion = 0;
    struct tabla carreras[54];
    almacenarmatriz(carreras);
    while(opcion != 4)//en la funcion main se hacen llamado de la mayoria de las funciones del codigo
    {
        opcion = menu();
        if (opcion == 1)
            ponderacioncarrera(carreras);
        if (opcion == 2)
            simularpostulacion(carreras);
        if (opcion == 3)
            consultafacultad(carreras);
    }
    printf("usted a salido del menu\n");
    return 0;
}
